## @file mainpage.py
#  Brief doc for mainpage.py
#  This documentation is for the MotorDriver class as defined by the guidelines provided in the Lab 1
#  handout for ME405.
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  Introduction of the Motor Driver class made during Lab 1.
#
#  @section sec_intro Introduction
#  This section currently only contains the Motor Driver class used to drive the IHM04A1 motor driver.  
#
# @section sec_rep Repository Link
# https://bitbucket.org/ChrisAEE/me405_labs/src/master/
#
#  @section sec_mot Motor Driver
#  The IHM04A1 motor driver is compatible with the Nucleo board used in ME405.
#
#  The following link is the datasheet of the motor driver: https://www.st.com/resource/en/data_brief/dm00213147.pdf
#
#  Please see the MotorDriver class, MotorDriver.py, for more information.
#
#
#  @section sec_enc Encoder Driver
#  WARNING: This section is currently incomplete.
#
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date April 24, 2020
#