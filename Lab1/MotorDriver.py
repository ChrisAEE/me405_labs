'''@file MotorDriver.py'''

"""
Created on Thu Apr  9 11:20:12 2020

@author: christianaguirre

This is the first class created to successfully interact with the Nucleo board
and the additional peripherals that come with the ME405 kit.
"""

import pyb
class MotorDriver:
    '''This class implements a motor driver (IHM04A1) to be used in conjunction with
        the ME405 Nucleo board'''
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        '''Creates a motor driver by initializing the following GPIO pins:
        @param EN_pin  A pyb.Pin object to use as the enable pin.
                       EN_pin is set high to enable or low to disable the motor driver.
        @param IN1_pin A pyb.Pin object to use as the IN1 (first input to the h-bridge) pin
        @param IN2_pin A pyb.Pin object to use as the IN2 (second input to the h-bridge) pin
        @param timer   A pyb.Timer object to use for the PWM generation on IN1_pin
                       and IN2_pin.
                       "timer" is also used to create objects for both channels of the timer:
                        timCh1 and timCh2.
        @param timCh1 A paramater that stores the value of channel 1 of timer.
        @param timCh2 A paramater that stores the value of channel 2 of timer.

           
        
                      '''
        
        print('Creating motor driver')
        
        #define the necessary class variables and store them
        self.EN_pin  = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        #Timer channels 1 and 2 are also defined to be used in later functions
        self.timCh1 = self.timer.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.timCh2 = self.timer.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)
        print('Successfully created motor driver')
        pass
        
    def enable(self):
        '''This method sets the EN_pin to high to enable the motor driver. '''        
        print('Enabling Motor')
        
        #Set the enable pin to high
        self.EN_pin.high()
        pass
        
    def disable(self):
        '''This method sets the EN_pin to high to enable the motor driver. '''
        print('Disabling Motor')
        
        #Set the enable pin to low
        self.EN_pin.low()
        pass
        
    def set_duty(self, duty):
        '''This method sets the duty cycle to besent to the motor to the given level. Positive values
        cause the effort in some positive direction, while negative values cause the effort to act
        in the opposite direction. The duty cycle is set to either timCh1 or timCh2, depending on the
        desired direction of the motor effort.
        
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor'''
        
        if duty >= 0: #Check to see if the duty cycle is positive or 0
            self.timCh1.pulse_width_percent(duty)
            self.timCh2.pulse_width_percent(0)
        elif duty < 0: #If not positive, then check to see if it is negative
            self.timCh1.pulse_width_percent(0)
            self.timCh2.pulse_width_percent(-duty)
        else: #Pass if duty cycle is an invalid input
            print('Invalid "duty" input, please try again')
            pass
        
        pass
    
    
'''
The main code for testing the newly defined MotorDriver class.
The code creates the necessary pin vairables to interface with the Nucleo board
and defines a timer object (channel 3) with a set frequency of 20 kHz.
To successfully operate the motor driver, the driver must be enabled using .enable(). 

@param pin_EN  A local object used to define the enable pin.
@param pin_IN1 A local object used to define the first h-bridge input pin.
@param pin_IN2 A local object used to define the second h-bridge input pin.
@param timmy   A local object used to define the timer associated to pins B4 and B5.
@param spinny  A local class used to define the motor driver. 

'''
if __name__ == '__main__':
    #Add comment here eventually
    
    #Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP) #
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4) #
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5) #
    
    #Create the timer object used for PWM Generation
    timmy  = pyb.Timer(3, freq = 20000) #Timer = timmy
    
    #Create a motor object using the earlier code
    spinny = MotorDriver(pin_EN, pin_IN1, pin_IN2, timmy)
    
    #Enable the motor driver
    spinny.enable()
    
    #Set the duty cycle to 10 percent
    spinny.set_duty(20)
    
    
    
    
    
    
    
    
    
    
    
    
