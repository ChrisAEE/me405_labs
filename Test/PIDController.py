'''@file PIDController.py'''

"""
Created on Tue May 12 2020

@author: christianaguirre

This is a PID controller class created to successfully drive a motor with encoder feedback,
per the requirements of the ME405 final project.
"""

import pyb

from motor import MotorDriver
from encoder import Encoder

class PID:
    '''This class implements a generic PID controller to accurately control a motor.'''
    
    def __init__ (self, Kp, Ki, Kd):
        '''Creates a proportional gain controller by initializing the following gain variable:
        @param Kp       The proportional gain of the controller. Determines dampness of the step-response.
                            Adjusting the Kp value higher produces an underdamped response, and lowering Kp
                            produces an overdamped response.
        
                      '''
        
        print('Creating controller')
        #initialize the necessary attributes and store them into self
        self.sampTime = 1000
        self.currTime = 0
        self.lastTime = 0
        
        self.error = 0
        self.tempError = 0
        self.errorSum = 0
        
        self.tempInput = 0
        
        self.kp  = Kp
        self.ki  = Ki*(sampTime/1000)
        self.kd  = Kd/(sampTime/1000)
        
        self.input = 0
        self.output = 0
        self.setpoint = 0
        
        self.outMax = 75
        self.outMin = -75
        

        print('Successfully created motor driver')
        pass
        
    def update(self, motorRef, encDelta):
        '''This method calculates the output of the gain controller using the setpoint and the encoder position. '''
        self.currTime = pyb.millis()
        if self.currTime - self.lastTime >= self.sampTime:
            error = motorRef - encDelta   #setpoint minus the input
            errorSum = errorSum + self.ki*error   #calculate sum of all error for integral
            if errorSum >= self.outMax:
                errorSum = self.outMax
            elif errorSum <= self.outMin:
                errorSum = self.outMax
            
            dInput = encDelta - self.tempInput  #calculate derivative, setpoint is constant then dError = -dInput
            
            
            out = self.kp*error + errorSum - self.kd*dInput
            
            self.tempInput = encDelta
            self.lastTime = self.currTime
        
            if out >= self.outMax:
                out = self.outMax
            elif out <= self.outMin:
                out = self.outMax
        else:
            
            
            
        return(out)
        pass
    
    def Gain(self, newKp):
        self.kp = newKp
        pass
    
    def Setpoint(self, newSet):
        self.setpoint = newSet
        pass
        
