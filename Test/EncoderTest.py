## @file encoder.py

"""
Created on Thu Apr  9 11:20:12 2020

@author: christianaguirre

This is the first class created to successfully interact with the Nucleo board
and the additional peripherals that come with the ME405 kit.
"""
import micropython
micropython.alloc_emergency_exception_buf(100)

import pyb


# class EncoderTest:
#     '''This class implements a  rotary encoder'''
#     
# 
#     
#     def __init__ (self, pinA, pinB):
#         '''Creates a motor driver by initializing the following GPIO pins:
#         @param pinA  A pyb.Pin object to use as the enable pin.
#                        EN_pin is set high to enable or low to disable the motor driver.
#         @param pinB  A pyb.Pin object to use as the IN1 (first input to the h-bridge) pin
# 
#                       '''
#         
#         print('Creating rotary encoder')
# 
#         
#         #define the necessary class variables and store them
#         self.pinA  = pinA
#         self.pinB  = pinB
#         
#         
# #         self.extint1 = pyb.ExtInt(self.pinA, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, callback1)
# #         self.extint2 = pyb.ExtInt(self.pinB, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, callback2)
#         
#         print('Successfully created motor driver')
#         
#         pass


temp = 100
counter = 100
    


def callback1(counter):
    if pyb.Pin.cpu.A5.value() == 0:
        counter = counter + 1
        
    elif pyb.Pin.cpu.A5.value() == 1:
        counter = counter - 1
        
    
   
    
def callback2(counter):
    if pyb.Pin.cpu.A6.value() == 0:
        counter = counter - 1
        
    elif pyb.Pin.cpu.A6.value() == 1:
        counter = counter + 1


        


if __name__ == '__main__':

    extint1 = pyb.ExtInt(pyb.Pin.cpu.A5, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, callback1)
    extint2 = pyb.ExtInt(pyb.Pin.cpu.A6, pyb.ExtInt.IRQ_RISING, pyb.Pin.PULL_UP, callback2)

#     #Create the pin objects used for interfacing with the motor driver
#     pin1  = pyb.Pin(pyb.Pin.cpu.A5)
#     #Create the pin objects used for interfacing with the motor driver
#     pin2  = pyb.Pin(pyb.Pin.cpu.A6)

    
    print(counter)
    

    

    
#     encoder = EncoderTest(pin1, pin2)
    
    while(1):
        if temp != counter:
            print(counter)
            temp = counter
        else:
            pass
    
    
    
