#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  6 12:11:58 2020

@author: christianaguirre
"""

'''@file ActualLab0.py
This file contains the code used to generate a Fibonacci Number from 
a desired index number inputted by a user. '''


def fib (idx):
    ''' This method calculates a Fibonacci number corresponding to
        a specified index.
        @param idx An integer specifying the index of the desired
            Fibonacci number.'''
            
    ##The following are vairables used to store the fibonacci numbers and the 
    #numbers used to calculate the fib. numbers
    add1 = 0 
    add2 = 0
    finNum = 0
    firstIn = 1
        
    ##Tell the user that their fibonacci number is being calculated
    print ('Calculating Fibonacci number at'
               ' index n = {:}.'.format(idx))
    
    ##The following loop calculates the Fibonacci number and stores the final
    #result into finNum
    for x in range(0,idx):
        if(x == 0 or x == 1):
            add2 = firstIn
        else:
            add1 = add2
            add2 = finNum
        finNum = add1 + add2
        
    return finNum #Return the final value

##Input command that prompts the user to input the desired index number or 
#to terminate the program
desIdx = input('Please input the desired Fibonacci Index or type "x" or "X" ' 
               'to exit the program: ')
        
if __name__ == '__main__':
    '''The main loop will continuously prompt the user to input a fibonacci 
    index. If the user enters an invalid input, the console will prompt the 
    user to input a different value. The loop will break, ending the 
    program, when an x or X is input. 
    '''
    while(True):
        try: 
            desIdxInt = int(desIdx)
            fibNum = fib(desIdxInt)
            print('Fib Num = {:}'.format(fibNum))
        except ValueError:
            if(desIdx == 'x' or desIdx == 'X'):
                break;
            else:
                print('Invalid Input! Please try again')
        desIdx = input('Please input the desired Fibonacci Index or type "x"' 
                       'or "X" to exit the program: ')
            

            
        