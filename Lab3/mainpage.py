## @file mainpage.py
#  This documentation is for the MotorDriver class as defined by the guidelines provided in the Lab 1
#  handout for ME405.
#
#  @mainpage
#  Updated project description with a new controller class file. The encoder class creates a usable controller objecect that stores a proportional
#  gain value, Kp. The class is capable of updating the motor driver PWM duty cycle value. The encoder and motor driver are used as the feedback and the
#  plant, respectively, in this lab.
#
# @section sec_vid Video Demo Link
#  https://youtu.be/JUlKgr_54UI
#
#
#  @section sec_intro Introduction
#  This repository documentation contains information concerning the implementation of the encoder and motor driver classes
#  previously created. The classes are using with a newly defined controller class to provide feedback control to the motor
#  position, theta.
#  
#
#  @section sec_Main Main
#  This file creates the encoder class, motor driver class, and controller class used for the proportional gain control of Lab 3.
#  The purpose of this lab is to implement all the previously designed classes into a single functional control system.
#
#  See main.py for a detailed explanation of the code and of the file. 
#
#
# @section sec_rep Repository Link
# https://bitbucket.org/ChrisAEE/me405_labs/src/master/
#
#  @section sec_mot Motor Driver
#  The IHM04A1 motor driver is compatible with the Nucleo board used in ME405.
#
#  The following link is the datasheet of the motor driver: https://www.st.com/resource/en/data_brief/dm00213147.pdf
#
#  Please see the MotorDriver class, motor.py, for more information.
#
#
#  @section sec_enc Encoder Driver
#
#  The encoder driver is the measurement device used to accurately track the position of the motor using quadrature signals. The A and B lines toggle between logic high and low
#  90 degrees out of phase to indicate movement in the positive or negative direction. The pyb timer class and counter method are used to set two timer channels to measure the encoder movement
#  and to count the clockwise and counterclock wise movement of the encoder.
#
#  Please see the Encoder class, encoder.py, for more information about the parameters used and the code written to test the encoder.
#
#
#  @section sec_con Controller
#
#  The controller designed during this lab implements a proportional gain controller. The controller measures the error between the encoder counts and the
#  actual position of the motor shaft and multiplies the error by the proportional gain, Kp. This simple feedback system is easy to implement but is not
#  as robust as other controllers due to the undesireable overshoot caused by the proportional gain and the poor steady-state error that is typically
#  associated with such a controller type.
#
#  The section below contains various step response plots gathered during the lab with varying Kp.
#
#  Please see controller.py for more information about the class.
#
#
#  @section sec_step Step Response of the Proportional Gain Controller
#
#  @image html StepResponses.jpg "Step Response (Encoder Counts) versus Time (milliseconds)" width=(50)%
#
#  @page BNO0555 Documentation
#  The following section details the procedure of testing the BNO055 IMU, including links to the
#  datasheet and the pyb documentation of the i2c class.
#
#  @section sec_bno BNO055
#
#  Using the BNO055 datasheet, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf,
#  the BNO class was created to interface with the IMU sensor using I2C. The device is initialized and
#  counter-offset to ensure the device euler angles are all initialized to 0. Once this is done, an update method
#  allows the microcontroller to update the euler angles by communicating directly to the sensors registers and reading the
#  angle measurements. A conversion factor is utilized to convert the 16-bit information into a useful angle for the user.
#
#  Please see i2c.py for more information about the code. 
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date May 20, 2020
#