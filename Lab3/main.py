# main.py -- put your code here!
## @file main.py
#  This file combines all of the previously made classes into a single experiment to control the position of a motor shaft.
#
#  This file creates an encoder class, a motor driver class and a controller class to implement feedback control of the motor shaft. The
#  motor driver drives the motor with the output of the controller, which is determined by the error of the encoder from the desired setpoint.
#  The user is prompted to input their desired gain and setpoint.  
#
#  @author Christian Aguirre
#
#

import pyb

from motor import MotorDriver
from encoder import Encoder
from controller import Controller



if __name__ == '__main__':
    '''#Main code for creating an encoder, motor driver and controller. The purpose of this section
    is to fully test each class and their ability to work with one another. '''
    
    #Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP) #
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4) #
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5) #
    
    #Create the timer object used for PWM Generation
    timmyM  = pyb.Timer(3, freq = 20000) #Timer = timmy
    
    #Create a motor object using the earlier code
    motomoto = MotorDriver(pin_EN, pin_IN1, pin_IN2, timmyM)
    
    #Disable the motor driver
    motomoto.enable()
    
    #Set the duty cycle to 10 percent
    motomoto.set_duty(0)
    
    #Create the pin objects used for interfacing with the encoder 1
    pin_encA = pyb.Pin(pyb.Pin.cpu.B6) #
    pin_encB = pyb.Pin(pyb.Pin.cpu.B7) #
    
    #Create the timer object used for encoder 1 counting
    timmyE  = pyb.Timer(4, prescaler=0, period=1000) #Timer 1 = timmy
    
    #Create an encoder class
    enc = Encoder(pin_encA, pin_encB, timmyE, 1000)
    
    #Define the setpoint and desired gain of the controller from the user
    desKpNew = input('Please input the desired Kp: ')
    desSet = input('Please input the desired setpoint: ')
    desKpint = 0
    
    
    #Loop this section of code with while(1)
    while(1):
        data = []    #Define the data and time arrays
        time = []
        
        #Convert the user input string into the gain and setpoint. returns error if invalid or terminates the program with an 'x'
        try: 
            desKpint = float(desKpNew)
            desSetint = int(desSet)
            control = Controller(desKpint)
            print("New Controller Made")
        except ValueError:
            if(desKp == 'x' or desKp == 'X'):
                break;
            else:
                print('Invalid Input! Please try again')
        
        #Initialize the count and error variables for data measuring
        count = 0
        dataCount = 0
        error = 0
        tempError = 0
        
        #Enable the motor and start timing the system
        motomoto.enable()
        start = pyb.micros()
        
        #Measure data until the encoder value stabilizes
        while(count <= 50):
            
            enc.get_delta()  #Measure the difference from the last position to the current
            duty = control.update(desSetint, enc.deltaSum) #Update the controller ouput
            
            motomoto.set_duty(duty)   #Set the duty cycle from the controllers output
            
            data.append(enc.deltaSum) # Append the data and time to their respective arrays
            time.append(pyb.elapsed_micros(start))
            
            #Calculate the error
            error = desSetint - enc.deltaSum 
            
            #Add to the count variable if the data has stabilized or reset counter otherwise
            if (tempError - error) == 0:
                count = count + 1
            elif (tempError - error) != 0:
                count = 0
            
            #Store the temporary error for the next cycle
            tempError = error
            
            #Add one to the array length
            dataCount = dataCount + 1
            
            #Small sampling delay to prevent overflow in data[]
            pyb.delay(1)
            
        #Clear the encoder deltaSum and disable the motor.    
        enc.clear_delta()
        motomoto.disable()
        
        #Print data 
        for n in range(dataCount):
            print('{:}, {:}'.format(time[n]/1000,data[n]))
        
        #Prompt user for new gain and setpoint
        desKpNew = input('Please input the desired Kp: ')
        desSet = input('Please input the desired setpoint: ')
        
        
        
