''' @file encoder.py
    
  This documentation is for the Encoder class as defined by the guidelines provided in the Lab 2
  handout for ME405.'''

import pyb

class Encoder:
    '''This class implements a magnetic encoder to be used in conjunction with
        the ME405 Nucleo board'''
    
    def __init__ (self, pinA, pinB, timer, period):
        '''Creates an encoder by initializing the following class attributes:
        @param pinA A pyb.Pin object to detect the logic level of the first encoder output.
        @param pinB A pyb.Pin object to detect the logic level of the second encoder output.
        @param timer   A pyb.Timer object to use for the encoder capture of pin's A and B.
                       "timer" is also used to create objects for both channels of the timer:
                        timCh1 and timCh2.
                        
        @param timCh1   A paramater that stores the counter value of channel 1 of timer.
        @param timCh2   A paramater that stores the counter value of channel 2 of timer.
        @param period   A parameter that stores the maximum value that the timer can count to before overflowing.
        @param thresh   A parameter that stores the threshold value of possible overflow. The maximum delta when an overflow
                          occurs shouldn't exceed this value.
                          
        @param counter  A parameter that stores the encoder's count value.
        @param temp     A parameter that stores the temporary counter value to distinguish between the current and previous value.
        @param overFt   A parameter that stores the delta value of the most recent update cycle of the encoder.
        @param deltaSum A parameter that stores the sum of all the deltas calculated. 
           
                      '''
        
        print('Creating motor encoder')
        
        self.temp     = 0
        self.counter  = 0
        self.overFt   = 0
        self.deltaSum = 0
        self.period   = period     #Maximum counter value
        self.thresh   = period/6   #Counter threshold for future overflow calculation
        
        #define the necessary class variables and store them
        self.pinA  = pinA
        self.pinB  = pinB
        self.timer = timer
        
        #Timer channels 1 and 2 are also defined to be used in later functions
        self.timCh1 = self.timer.channel(1, pyb.Timer.ENC_A, pin = self.pinA)
        self.timCh2 = self.timer.channel(2, pyb.Timer.ENC_B, pin = self.pinB)
        
        self.timer.counter(0)   #set the timer counter to 0 during initialization
        
        print('Successfully created the encoder class')
        pass
        
    def update(self):
        '''Updates the position of the encoder's counter and temporary counter using the
        timers built in counter. '''
        
        if(self.temp!=self.counter):
            self.flow()
            self.counter = self.temp
        self.temp = self.timer.counter()
        pass
    
    def get_position(self):
        '''Returns the position of the encoder's counter.'''
        return self.counter
        
    def set_position(self, counterVal):
        '''Sets the counter value of the encoder from a user input.
        @param counterVal An integer that is used to set the counter value of the timer.'''
        self.timer.counter(counterVal)
        pass
        
    
    def clear_delta(self):
        '''Clears the sum of the delta stored as an attribute of the encoder class.'''
        self.deltaSum = 0
        pass
    
    def get_delta(self):
        '''Updates the position of the encoder's counter using the update() function and
        updates the delta sum of the encoder. The delta value is also reset.'''
        self.update()
        self.deltaSum = self.deltaSum + self.overFt 
        loc = self.overFt
        self.overFt = 0
        return(loc)
        
    def flow(self):
        '''The encoder counter value is checked for overflow using the period and the threshold attributes
        defined earlier. The delta of the most recent update is calculated differently if an overflow is found and regularly
        if no overflow is found. '''
        if (self.temp <= self.period and self.temp >= self.period-self.thresh) and (self.counter >= 0 and self.counter <= self.thresh):
            self.overFt = -self.period + self.temp - self.counter
        elif (self.counter <= self.period and self.counter >= self.period-self.thresh) and (self.temp >= 0 and self.temp <= self.thresh):
            self.overFt = self.period + self.temp - self.counter
        else:
            self.overFt = self.temp - self.counter
        pass
        
    def speed(self, samp):
        '''Calculate the speed of rotation of the encoder.
        '''
        sampS = samp/1000
        speed = self.get_delta()/sampS
        return(speed)



# if __name__ == '__main__':
#     '''#Main code for creating an encoder class and verifying the functionality
#     #of the user created code. The code tests two separate encoders individually
#     #and ensures that both operate similarly but using two separate pairs of pins
#     #and timers. '''
#     
#     #Create the pin objects used for interfacing with the encoder 1
#     pin_encA = pyb.Pin(pyb.Pin.cpu.B6) #
#     pin_encB = pyb.Pin(pyb.Pin.cpu.B7) #
#     
#     #Create the timer object used for encoder 1 counting
#     timmy  = pyb.Timer(4, prescaler=0, period=1000) #Timer 1 = timmy
#     
#     #Create an encoder class
#     enc = Encoder(pin_encA, pin_encB, timmy, 1000)
#     
#     #Create the pin objects used for interfacing with the encoder 2
#     pin_encA2 = pyb.Pin(pyb.Pin.cpu.A5) #
#     pin_encB2 = pyb.Pin(pyb.Pin.cpu.B3) #
#     
#     #Create the timer object used for encoder 2 counting
#     timmy2  = pyb.Timer(2, prescaler=0, period=1000) #Timer 2 = timmy2
#     
#     #create the second encoder class
#     enc2 = Encoder(pin_encA2, pin_encB2, timmy2, 1000)
#         
#     sampTime = 100 #sample time in millisecond
#     
#     while(1):
#         enc1val = enc.get_delta()  #Measure the difference from the last position to the current 
#         enc2val = enc2.get_delta() # for encoders 1 and 2
#         
#         if(enc2val != 0):
#             print(enc2.deltaSum)  #Print encoder 1 and 2 overall position
#         if(enc1val != 0):
#             print(enc.deltaSum)
# 
#         

            
            
            
        
        

    
    

    

    
    