'''@file controller.py'''

"""
Created on Tue May 12 2020

@author: christianaguirre

This is a controller class created to successfully interact a motor through a motor driver,
an encoder and the nucleo board per the specifications of Lab 3 for ME405.
"""

import pyb

from motor import MotorDriver
from encoder import Encoder

class Controller:
    '''This class implements a generic proportional gain controller to test the step response of the motor.'''
    
    def __init__ (self, Kp):
        '''Creates a proportional gain controller by initializing the following gain variable:
        @param Kp       The proportional gain of the controller. Determines dampness of the step-response.
                            Adjusting the Kp value higher produces an underdamped response, and lowering Kp
                            produces an overdamped response.
        
                      '''
        
        print('Creating controller')
        
        #define the necessary class attributes and store them
        self.kp  = Kp

        print('Successfully created motor driver')
        pass
        
    def update(self, motorRef, encDelta):
        '''This method calculates the output of the gain controller using the setpoint and the encoder position. '''        
        error = motorRef - encDelta
        out = self.kp*error
        
        if out >= 75:
            out = 75
        elif out <= -75:
            out = -75
            
            
        return(out)
        pass
    
    def Gain(self, newKp):
        self.kp = newKp
        pass
        