# main.py -- put your code here!
## @file main.py
#  This file is responsible for initializing each pin, each class and running a loop that prompts a user for input. The loop can be stopped by inputting 'x'.
#
#  @author Christian Aguirre
#
#
#

import pyb

from motor import MotorDriver
from encoder import Encoder
from controller import Controller
from PIDController import PID



print("Worked")



if __name__ == '__main__':
    '''#Main code for creating an encoder class and verifying the functionality
    #of the user created code. The code tests two separate encoders individually
    #and ensures that both operate similarly but using two separate pairs of pins
    #and timers. '''
    
    #Create the pin objects used for interfacing with the motor driver
    pin_EN  = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP) #
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4) #
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5) #
    
    
    #Create the timer object used for PWM Generation
    timmyB  = pyb.Timer(3, freq = 20000) #Timer = timmy
    
    #Create a motor object using the earlier code
    motoBerry = MotorDriver(pin_EN, pin_IN1, pin_IN2, timmyB)
    
    #Disable the motor driver
    motoBerry.enable()
    
    #Set the duty cycle to 10 percent
    motoBerry.set_duty(0)
    
    
    #Create the pin objects used for interfacing with the motor driver
    pin_ENB  = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP) #
    pin_INB1 = pyb.Pin(pyb.Pin.cpu.A0) #
    pin_INB2 = pyb.Pin(pyb.Pin.cpu.A1) #
    
    #Create the timer object used for PWM Generation
    timmyM  = pyb.Timer(5, freq = 20000) #Timer = timmy
    
    #Create a motor object using the earlier code
    motoMnM = MotorDriver(pin_ENB, pin_INB1, pin_INB2, timmyM)
    
    #Disable the motor driver
    motoMnM.enable()
    
    #Set the duty cycle to 10 percent
    motoMnM.set_duty(0)
    

    
    #Create the pin objects used for interfacing with the encoder 1
    pin_encA = pyb.Pin(pyb.Pin.cpu.B6) #
    pin_encB = pyb.Pin(pyb.Pin.cpu.B7) #
    
    #Create the pin objects used for interfacing with the encoder 2
    pin_encA2 = pyb.Pin(pyb.Pin.cpu.A5) #yellow
    pin_encB2 = pyb.Pin(pyb.Pin.cpu.B3) #green
    
    #Create the timer object used for encoder 1 counting
    timmyE  = pyb.Timer(4, prescaler=5, period=1000) #Timer 1 = timmy
    
    #Create an encoder class
    enc = Encoder(pin_encA, pin_encB, timmyE, 1000)
    
    #Create the timer object used for encoder 2 counting
    timmy2  = pyb.Timer(2, prescaler=5, period=1000) #Timer 2 = timmy2
    
    #create the second encoder class
    enc2 = Encoder(pin_encA2, pin_encB2, timmy2, 1000)
    
    desBerry = input('Please input the desired First Candy ("l", "m", "h"): ')
    desMnM = input('Please input the desired Second Candy ("l", "m", "h"): ')
    
    KpB = 0
    KpM = 0
    scale = 0.78;
    
    delayTime = 1
    
    
    while(1):
        data = []
        time = []
        
        if(desBerry == 'x' and desMnm == 'x'):
            break
        
        if(desBerry == 'l'):
            SB = 20
        elif(desBerry == 'm'):
            SB = 27
        elif(desBerry == 'h'):
            SB = 35
        else:
            SB = 0
            
        if(desMnM == 'l'):
            SM = 20
        elif(desMnM == 'm'):
            SM = 27
        elif(desMnM == 'h'):
            SM = 35
        else:
            SM = 0
        
                
        controlB = PID(8.3, 5, 1)
        controlM = PID(9, 5, 1)
        
        print("New Controllers Made")
        
        desSetintB = SB
        desSetintM = SM
        countB = 0
        dataCountB = 0
        errorB = 0
        tempErrorB = 0
        
        motoBerry.enable()
        motoMnM.enable()
        
        start = pyb.micros()
        
        while(countB <= 500):

            enc.get_delta()  #Measure the difference from the last position to the current
            dutyB = controlB.update(desSetintB, enc.deltaSum)
            
            enc2.get_delta()  #Measure the difference from the last position to the current
            dutyM = controlM.update(desSetintM, enc2.deltaSum)
            motoBerry.set_duty(dutyB)
            motoMnM.set_duty(dutyM)

            
            errorB = desSetintB - enc.deltaSum 
            
            if (tempErrorB - errorB) == 0:
                countB = countB + 1
            elif (tempErrorB - errorB) != 0:
                countB = 0
            
            tempErrorB = errorB
            dataCountB = dataCountB + 1
            pyb.delay(delayTime)
            
        desSetintB = -9
        desSetintM = -1
        countB = 0
        dataCountB = 0
        errorB = 0
        tempErrorB = 0

        while(countB <= 500):
            
            enc.get_delta()  #Measure the difference from the last position to the current
            dutyB = controlB.update(desSetintB, enc.deltaSum)
            
            enc2.get_delta()  #Measure the difference from the last position to the current
            dutyM = controlM.update(desSetintM, enc2.deltaSum)
            motoBerry.set_duty(dutyB)
            motoMnM.set_duty(dutyM)
            
            errorB = desSetintB - enc.deltaSum 
            
            if (tempErrorB - errorB) == 0:
                countB = countB + 1
            elif (tempErrorB - errorB) != 0:
                countB = 0
            
            tempErrorB = errorB
            dataCountB = dataCountB + 1
            pyb.delay(delayTime)
            
            
        print(enc.deltaSum)
        print(enc2.deltaSum)
        
        enc.clear_delta()
        motoBerry.disable()
        
        enc2.clear_delta()
        motoMnM.disable()

        

        desBerry = input('Please input the desired Berries ("l", "m", "h"): ')
        desMnm = input('Please input the desired MnMs ("l", "m", "h"): ')
       
        
        

