'''@file PIDController.py'''

"""
Created on Tue May 12 2020

@author: christianaguirre

This is a PID controller class created to successfully drive a motor with encoder feedback,
per the requirements of the ME405 final project.
"""

import pyb

from motor import MotorDriver
from encoder import Encoder

class PID:
    '''This class implements a generic PID controller to accurately control a motor.'''
    
    def __init__ (self, Kp, Ki, Kd):
        '''Creates a proportional gain controller by initializing the following gain variable:
        @param Kp       The proportional gain of the controller. Determines dampness of the step-response.
                            Adjusting the Kp value higher produces an underdamped response, and lowering Kp
                            produces an overdamped response.
        @param Ki       The integral gain of the controller. Drives the system to 0% steady-state error, which is important
                            when dealing with precise control, such as a candy dispenser. 
        @param Kd       The proportional gain of the controller. Predicts the future movement of the actuators through
                            the derivative.
                            
        @param sampTime   Define a sampling time (in ms) to limit the number of calculations perform and ensure uniform time distribution of sampling.
        @param currTime   A paramater to store the current time into.
        @param lastTime   A parameter to store the previous calculation time into.
        
        @param error       A parameter that stores the error (Setpoint minus the input).
        @param tempError   A parameter that stores the temporary error value.
        @param errorSum    A parameter that stores the total error sum (errorSum += error*Ki).
        
        @param tempInput   A parameter that stores the previous input value for the derivative term calculation.
        @param Input       A parameter that stores the input value if needed. 
        @param Output      A parameter that stores the output value of the controller.
        @param Setpoint    A parameter that stores the setpoint value if needed. 
        
        @param outMax   A parameter that defines the output maximum which will later be scaled down to produce the PWM duty cycle %
        @param outMin   A parameter that defines the output minimum which will later be scaled down to produce the PWM duty cycle %
 
        
                      '''
        
        print('Creating controller')
        #initialize the necessary attributes and store them into self
        self.sampTime = 150   #Define a sampling time (in ms) to limit the number of calculations perform and ensure uniform time distribution of sampling.
        self.currTime = 0     #variable to store the current time into.
        self.lastTime = 0     #variable to store the previous calculation time into.
        
        self.error = 0        #Variable that stores the error (Setpoint minus the input)
        self.tempError = 0    #Stores the temporary error value.
        self.errorSum = 0     #Stores the total error sum (errorSum += error*Ki)
        
        self.tempInput = 0    #Stores the previous input value for Kd
        
        self.kp  = Kp         #Define the proportional, integral and derivative gains with the sampling time considered
        self.ki  = Ki*(self.sampTime/1000)
        self.kd  = Kd/(self.sampTime/1000)
        
        self.input = 0        #Initialize the input, output and setpoint 
        self.output = 0
        self.setpoint = 0
        
        self.outMax = 1125     #Define the output maximum and minimum which will later be scaled down to produce the PWM duty cycle %
        self.outMin = -1125
        

        print('Successfully created PID Controller')
        pass
        
    def update(self, motorRef, encDelta):
        '''This method calculates the output of the PID controller using the setpoint (motorRef) and the encoder position (encDelta). '''
        self.currTime = pyb.millis()  #Stores the current time to check if it is time for a new calculation
        if self.currTime - self.lastTime >= self.sampTime:
            self.error = motorRef - encDelta   #setpoint minus the input
            self.errorSum = self.errorSum + self.ki*self.error*(self.sampTime/1000)   #calculate sum of all error for integral
            
            if self.errorSum >= self.outMax:
                self.errorSum = self.outMax
            elif self.errorSum <= self.outMin:
                self.errorSum = self.outMin
            
            dInput = encDelta - self.tempInput  #calculate derivative, setpoint is constant then dError = -dInput
            
            
            out = self.kp*self.error + self.errorSum - self.kd*dInput/(self.sampTime/1000)
            
            self.tempInput = encDelta
            self.lastTime = self.currTime
        
            if out >= self.outMax:
                out = self.outMax
            elif out <= self.outMin:
                out = self.outMin
            self.output = int(out/13.3333)
        else:
            pass
            
        return(self.output)
        
    
    def Gain(self, newKp):
        '''This method defines a new proportional gain for the controller to use. '''
        self.kp = newKp
        pass
    
        
