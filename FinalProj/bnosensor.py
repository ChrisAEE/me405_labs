'''@file bnosensor.py


This is a class created to easily use the BNO055 IMU sensor, as per the instructions of Lab 4. The primary methods update and store the euler angles of the device. 
'''

import pyb
import utime

class BNO:
    '''This class defines a BNO055 IMU sensor. The sensor is fully compatible with any microcontroller capable of communicating using I2C.'''
    
    def __init__ (self, ADDR, i2c):
        '''Creates a BNO sensor class by initializing the following parameters (all registers and modes are with respect to the BNO055 datasheet):
        @param ADDR     The address of the BNO055.
        @param pwrMode  The desired normal power mode for the sensor to operate.
        @param pwrADDR  The power mode register.
        @param oprMode  The desired operating mode for the sensor to run.
        @param oprADDR  The operating mode register.
        
        @param delay    Delay to ensure receiving and transmitting are properly spaced out.
        @param conv     Conversion rate for 16-bit numbers to their corresponding decimal value. 
        @param gyroLSB  The lower byte of gyroscope values.
        @param gyroMSB  The upper byte of gyroscope values.
        @param yaw/roll/pitchLSB  The lower byte of euler angle values.
        @param yaw/roll/pitchMSB  The upper byte of euler angle values.
                      '''
        
        print("Creating device")
        self.ADDR = ADDR
        
        self.pwrMode = 0x00
        self.pwrADDR = 0x3E
        self.oprMode = 0x0C
        self.oprADDR = 0x3D
        
        self.delay = 5
        self.conv = 16384/900/1.14
        
        self.gyroXLSB = 0x14
        self.gyroXMSB = 0x15
        self.gyroYLSB = 0x16
        self.gyroYMSB = 0x17
        self.gyroZLSB = 0x18
        self.gyroZMSB = 0x19
        
        self.yawLSB = 0x1A
        self.yawMSB = 0x1B
        self.rollLSB = 0x1C
        self.rollMSB = 0x1D
        self.pitchLSB = 0x1E
        self.pitchMSB = 0x1F
        
        self.I2C = i2c
        self.pitch = 0
        self.roll = 0
        self.yaw = 0
        
        self.pitch1 = self.readEul(self.pitchMSB, self.pitchLSB)
        self.roll1 =  self.readEul(self.rollMSB, self.rollLSB)
        self.yaw1 =   self.readEul(self.yawMSB, self.yawLSB)

        self.I2C.mem_write(self.pwrMode, self.ADDR, self.pwrADDR)
        self.I2C.mem_write(self.oprMode, self.ADDR, self.oprADDR)
        
        print("Successfully created device")
        pass
        
    def updateEulAng(self):
        '''This method updates the euler angle parameters of the class.'''
        self.pitch = self.readEul(self.pitchMSB, self.pitchLSB) - self.pitch1
        self.roll =  self.readEul(self.rollMSB, self.rollLSB) - self.roll1
        self.yaw =   self.readEul(self.yawMSB, self.yawLSB) - self.yaw1
        pass

        
    def readEul(self, reg1, reg2):
        '''This method reads the euler angle registers and converts the 16-bit values into a decimal value.'''
        self.data1 = bytearray(1)  # create multiple byte buffers
        self.data2 = bytearray(1)  
        self.I2C.mem_read(self.data1, self.ADDR, reg1, timeout=5000, addr_size=8)
        utime.sleep_ms(self.delay)
        self.I2C.mem_read(self.data2, self.ADDR, reg2, timeout=5000, addr_size=8)
        utime.sleep_ms(self.delay)
        
        
        sub = 0    
        if(self.data1[0] >= 0x80):
            sub = 32768*2
        
        temp = (self.data1[0]<<8) + self.data2[0] - sub    
        temp = (temp+0.2)/self.conv
        return temp
        pass
        
        

# ADDR = 0x28
# i2c = pyb.I2C(1, pyb.I2C.MASTER, baudrate = 100000)
# 
# imu = BNO(ADDR, i2c)
# print(i2c.mem_read(6, 40, 0x61, timeout=5000, addr_size=8))
# 
# while(1):
#     imu.updateEulAng()      #update angle
#     pitch = int(imu.pitch)  #store imu euler angles into local variables
#     roll = int(imu.roll)
#     yaw = int(imu.yaw)
#     
#     print('Pitch: ' + str(pitch))  #print all euler angles
#     print('Roll: ' + str(roll))
#     print('Yaw: ' + str(yaw))
#     
#     utime.sleep_ms(50)     #delay to prevent console flood
#     