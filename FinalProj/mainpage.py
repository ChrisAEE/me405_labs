## @file mainpage.py
#  This documentation is for the completed ME405 course, including code for: a motor driver, encoder, IMU, and the final project. 
#
#  @mainpage
#  Final update: This documentation includes all previously created classes and a newly implemented PID controller to drive the PWM signal of a motor. The
#  final project page details the newly implemented PID Controller and the requirements of the final project as defined during the project proposal. 
#
# @section sec_vid Final Project Demo Link
#  https://youtu.be/NQ-moYX3qPI
#
#
#  @section sec_intro Introduction
#  This repository documentation contains information concerning the implementation of the encoder, motor driver, IMU and proportional gain controller classes
#  previously created. 
#
#  The most recent addition to this documentation is the newly created final project page that details the creation of a Dual-Candy Dispenser.
#
#
#
# @section sec_rep Repository Link
# https://bitbucket.org/ChrisAEE/me405_labs/src/master/
#
#  @section sec_mot Motor Driver
#  The IHM04A1 motor driver is compatible with the Nucleo board used in ME405.
#
#  The following link is the datasheet of the motor driver: https://www.st.com/resource/en/data_brief/dm00213147.pdf
#
#  Please see the MotorDriver class, motor.py, for more information.
#
#
#  @section sec_enc Encoder Driver
#
#  The encoder driver is the measurement device used to accurately track the position of the motor using quadrature signals. The A and B lines toggle between logic high and low
#  90 degrees out of phase to indicate movement in the positive or negative direction. The pyb timer class and counter method are used to set two timer channels to measure the encoder movement
#  and to count the clockwise and counterclock wise movement of the encoder.
#
#  Please see the Encoder class, encoder.py, for more information about the parameters used and the code written to test the encoder.
#
#  @section sec_bno BNO055
#
#  The following section details the procedure of testing the BNO055 IMU, including links to the
#  datasheet and the pyb documentation of the i2c class.
#
#  Using the BNO055 datasheet, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf,
#  the BNO class was created to interface with the IMU sensor using I2C. The device is initialized and
#  counter-offset to ensure the device euler angles are all initialized to 0. Once this is done, an update method
#  allows the microcontroller to update the euler angles by communicating directly to the sensors registers and reading the
#  angle measurements. A conversion factor is utilized to convert the 16-bit information into a useful angle for the user.
#
#  Please see bnosensor.py for more information about the code.
#
#  No main python file was used for the purpose of this lab. The code was tested directly from bnosensor.py.
#
#
#  @section sec_con Controller
#
#  The controller designed during this lab implements a proportional gain controller. The controller measures the error between the encoder counts and the
#  actual position of the motor shaft and multiplies the error by the proportional gain, Kp. This simple feedback system is easy to implement but is not
#  as robust as other controllers due to the undesireable overshoot caused by the proportional gain and the poor steady-state error that is typically
#  associated with such a controller type.
#
#  The section below contains various step response plots gathered during the lab with varying Kp.
#
#  Please see controller.py for more information about the class.
#
#
#  @section sec_step Step Response of the Proportional Gain Controller
#
#  @image html StepResponses.jpg "Step Response (Encoder Counts) versus Time (milliseconds)" width=(50)%
#
#
#
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date June 12, 2020
#
#  @page term_prj Final Project Documentation
#  This page details the creation, requirements and documentation associated with the final project for ME405.
#
#  Demo Link: https://youtu.be/NQ-moYX3qPI
#
#  @section term_prj_prop Project Proposal for Dual Candy Dispenser
#
#  The purpose of this project is to create a functional dual candy dispenser, capable of outputing varying amounts of candy according to user input. 
#
#  This project will integrate the use of two actuators, likely the motors used during the labs, two encoders to provide feedback, and PI feedback control. The motors will actuate and allow candy to fall while the encoders return the position of the motors. 
#
#  The decision to implement PI control is necessary, as the dispenser must be fully closed to ensure no candy escapes the holding compartment when the device is not being used. This means that 0% steady state error is allowed.
#  The PI control is the 'new' component to this project, which can be upgraded to PID control if the complexity of the project with only PI control is not enough.
#
#
#  The mechanical structure of this project will likely be made of cardboard, which is easily cut with a knife and connected using hot glue, all of which I am currently in possession of. 
#
#  It would ideally take one week to build the scale and interconnect the parts, while waiting for the glue to dry, one week to finalize the code, including the addition of a the new controller, and half a week to troubleshoot once everything is built.
#  
#  The most dangerous part of this project will be working with hot glue, which I can wear safety goggles and gloves for.
#
#  @section reqs Requirements
#
#  As detailed in the project proposal, this project will require the addition of a newly created PID Controller class. It will utilize two actuators to each individually control one candy dispenser. The actuator feedback will be provided
#  by the encoders attached to each motor. The encoder signal will act as the feedback from the output of the control system to the input of the newly defined PID controller. The PID controller will be robust enough to handle
#  varying setpoints while implementing output limits to prevent the motors from running at 100% capacity. The derivative portion of the controller will calculate the derivative of the input to prevent derivative spiking, while
#  the integral portion of the controller will use the sum of an integral term rather than simply accumulating error and multiplying the error sum by the integral gain. This is done because the integral term is the most sensitive to
#  the changing of gain as normally it multiplies the total error by a single gain. These design considerations will allow the controller to be robust enough to operate as an independent module rather than a hard-coded solution for this project.
#
#  The code also processes user input to define the amount of candy to dispense from each dispenser.
#
#  The candy dispenser was assembled using cardboard, hot glue and zipties. 
#
#  @section pidc PID Controller
#
#  The PID Controller designed features its own sampling time to simplify the calculations performed by the class. By sampling at uniform rates, the integral and derivative sums become much easier to compute. The integral gain is simply
#  Ki * (sampling time in seconds) and the derivative term is Kd / (sampling time in seconds). The output of the controller is limited to 85% and -85% PWM to prevent the motors from running dangerously at 100%.
#
#  The controller stores the previous calculation time and constantly verifies that the current time is at least the previous calculation time plus the sampling time. The error is also stored to calculate the proportional portion of the PID and to store the
#  total error times the integral gain into a single integral term.
#
#  Since the derivative term is the derivative of the error times the derivative gain, and the error is simply the setpoint minus the input, the derivative portion of the controller can safely assume that the setpoint is constant through its
#  standard operation and only calculates the derivative of the input, the signal from the encoder.
#
#  The output of the controller is then the error times the proportional gain, Kp, plus the sum of the error with the integral gain, Ki, accounted for plus the derivative of the input times the derivative gain, Kd.
#
#  For more detailed information about this implementation, please see PIDController.py and the derivation of the equations below.
#
#  @image html PIDEquation.png 
#
#  @section sec_sys Simple Control System Model
#
#  @image html ControlSystem.png "Dual Control System with Individual PID Control per Actuator" width=(50)%
#
#  @section main Main
#
#  The main.py file is responsible for initializing each pin on the microcontroller and creating the desired classes (2 PID controllers, 2 motor drivers, 2 encoders). The main file will prompt the user for input
#  upon successfully creating each class, will then run the control loops with predefined setpoints and will finish by prompting the user for another input. The program can be closed at any time by inputting 'x' into
#  the input prompt. 
#
#  @section chall Challenges
#
#  Unfortunately, the PID controller was crudely tuned, meaning that the best performance would have only been found after many hours of iterative gain changing, which would have been too time consuming.
#  The performance of the final project was grealy hindered due to the unstable nature of the motor mounting and the overall mechanical structure of the dispensers. While the performance of the actual candy dispensing needs refining,
#  both fom the PID controller and a revamp to the mechanical structure, the PID controller displays its robustness through rapid convergence and nearly 0% steady state error. With minor modifications and more time,
#  this project could be tidied up and possibly sold around stores nationwide as a cheap candy dispensing method (probably not but a man can dream).
#
#  Additionally, it was difficult coming up with a suitable container to store the candy that wouldn't seize up during the demo. During the demo, the dispensers were filled with a minimal amount of candy to show
#  the proof of concept working, but it the dispensers were over filled, then the candy would stop dispensing. This could be fixed with a different storage mechanism. 
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date June 12, 2020





