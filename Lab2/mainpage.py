## @file mainpage.py
#  Brief doc for mainpage.py
#  This documentation is for the MotorDriver class as defined by the guidelines provided in the Lab 1
#  handout for ME405.
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#  Updated project description with a new encoder class file. The encoder class creates a usable encoder object to use with the Nucleo board. The class is capable of
#  updating the encoder position without the possibility of overflow, and can clear/set the counter value as needed. The encoder is meant to perform concurrently with the
#  motor driver class created during Lab 1. 
#
#  @section sec_intro Introduction
#  This repository documentation contains information concerning the encoder and motor driver classes created
#  for lab's 1 and 2 of ME405.
#
# @section sec_rep Repository Link
# https://bitbucket.org/ChrisAEE/me405_labs/src/master/
#
#  @section sec_mot Motor Driver
#  The IHM04A1 motor driver is compatible with the Nucleo board used in ME405.
#
#  The following link is the datasheet of the motor driver: https://www.st.com/resource/en/data_brief/dm00213147.pdf
#
#  Please see the MotorDriver class, MotorDriver.py, for more information.
#
#
#  @section sec_enc Encoder Driver
#
#  The encoder driver is the measurement device used to accurately track the position of the motor using quadrature signals. The A and B lines toggle between logic high and low
#  90 degrees out of phase to indicate movement in the positive or negative direction. The pyb timer class and counter method are used to set two timer channels to measure the encoder movement
#  and to count the clockwise and counterclock wise movement of the encoder.
#
#  Please see the Encoder class, Encoder.py, for more information about the parameters used and the code written to test the encoder. 
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date April 29, 2020
#