## @file mainpage.py
#  This documentation is for the completed ME405 course, including code for: a motor driver, encoder, IMU, and the final project. 
#
#  @mainpage
#  Final update: This documentation includes all previously created classes and a newly implemented PID controller to drive the PWM signal of a motor. The
#  final project page details the newly implemented PID Controller and the requirements of the final project as defined during the project proposal. 
#
# @section sec_vid Final Project Demo Link
#  
#
#
#  @section sec_intro Introduction
#  This repository documentation contains information concerning the implementation of the encoder, motor driver, IMU and proportional gain controller classes
#  previously created. 
#
#  The most recent addition to this documentation is the newly created final project page that details the creation of a Dual-Candy Dispenser.
#
#
#
# @section sec_rep Repository Link
# https://bitbucket.org/ChrisAEE/me405_labs/src/master/
#
#  @section sec_mot Motor Driver
#  The IHM04A1 motor driver is compatible with the Nucleo board used in ME405.
#
#  The following link is the datasheet of the motor driver: https://www.st.com/resource/en/data_brief/dm00213147.pdf
#
#  Please see the MotorDriver class, motor.py, for more information.
#
#
#  @section sec_enc Encoder Driver
#
#  The encoder driver is the measurement device used to accurately track the position of the motor using quadrature signals. The A and B lines toggle between logic high and low
#  90 degrees out of phase to indicate movement in the positive or negative direction. The pyb timer class and counter method are used to set two timer channels to measure the encoder movement
#  and to count the clockwise and counterclock wise movement of the encoder.
#
#  Please see the Encoder class, encoder.py, for more information about the parameters used and the code written to test the encoder.
#
#  @section sec_bno BNO055
#
#  The following section details the procedure of testing the BNO055 IMU, including links to the
#  datasheet and the pyb documentation of the i2c class.
#
#  Using the BNO055 datasheet, https://www.bosch-sensortec.com/media/boschsensortec/downloads/datasheets/bst-bno055-ds000.pdf,
#  the BNO class was created to interface with the IMU sensor using I2C. The device is initialized and
#  counter-offset to ensure the device euler angles are all initialized to 0. Once this is done, an update method
#  allows the microcontroller to update the euler angles by communicating directly to the sensors registers and reading the
#  angle measurements. A conversion factor is utilized to convert the 16-bit information into a useful angle for the user.
#
#  Please see bnosensor.py for more information about the code.
#
#  No main python file was used for the purpose of this lab. The code was tested directly from bnosensor.py.
#
#
#  @section sec_con Controller
#
#  The controller designed during this lab implements a proportional gain controller. The controller measures the error between the encoder counts and the
#  actual position of the motor shaft and multiplies the error by the proportional gain, Kp. This simple feedback system is easy to implement but is not
#  as robust as other controllers due to the undesireable overshoot caused by the proportional gain and the poor steady-state error that is typically
#  associated with such a controller type.
#
#  The section below contains various step response plots gathered during the lab with varying Kp.
#
#  Please see controller.py for more information about the class.
#
#
#  @section sec_step Step Response of the Proportional Gain Controller
#
#  @image html StepResponses.jpg "Step Response (Encoder Counts) versus Time (milliseconds)" width=(50)%
#
#
#
#
#  @author Christian Aguirre
#
#  @copyright Currently un-licensed but please don't steal this work. 
#
#  @date May 20, 2020
#
#
#  @page term_prj Project Proposal
#
#  @section proj Dual Candy Dispenser
#
#  The purpose of this project is to create a functional dual candy dispenser, capable of outputing varying amounts of candy according to user input. 
#
#  This project will integrate the use of two actuators, likely the motors used during the labs, two encoders to provide feedback, and PI feedback control. The motors will actuate and allow candy to fall while the encoders return the position of the motors. 
#
#  The decision to implement PI control is necessary, as the dispenser must be fully closed to ensure no candy escapes the holding compartment when the device is not being used. This means that 0% steady state error is allowed.
#  The PI control is the 'new' component to this project, which can be upgraded to PID control if the complexity of the project with only PI control is not enough.
#
#
#  The mechanical structure of this project will likely be made of cardboard, which is easily cut with a knife and connected using hot glue, all of which I am currently in possession of. 
#
#  It would ideally take one week to build the scale and interconnect the parts, while waiting for the glue to dry, one week to finalize the code, including the addition of a the new controller, and half a week to troubleshoot once everything is built.
#  
#  The most dangerous part of this project will be working with hot glue, which I can wear safety goggles and gloves for. 
#




